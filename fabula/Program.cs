﻿using System.CommandLine;
using System.CommandLine.Invocation;
using System.Reflection;

namespace Fabula
{
    public class Program
    {
        private readonly static Option<DirectoryInfo> OutputDirectoryOption = new("--output", getDefaultValue: () => new DirectoryInfo("output"), description: "Directory to output static site to");
        private readonly static Option<string> SiteGeneratorOption = new("--site-generator", description: "Assembly name for a site generator");
        private readonly static Option<DirectoryInfo> TemplatePath = new("--templates", getDefaultValue: () => new DirectoryInfo("templates"), description: "Where to search for templates");

        public static int Main(string[] args)
        {
            var command = new RootCommand("A static site generator") { OutputDirectoryOption, SiteGeneratorOption, TemplatePath };
            command.SetHandler(RootCommand);
            return command.Invoke(args);
        }

        private static void RootCommand(InvocationContext handle)
        {
            var outputDirectory = handle.ParseResult.GetValueForOption(OutputDirectoryOption);
            if (outputDirectory == null) throw new Exception("--output has not been defined");
            var siteGenerator = handle.ParseResult.GetValueForOption(SiteGeneratorOption);
            if (string.IsNullOrWhiteSpace(siteGenerator)) throw new Exception("--site-generator has not been defined");

            var assembly = Assembly.Load(siteGenerator);
            var siteGen = new SiteGenerationProcess(assembly);
        }
    }
}
