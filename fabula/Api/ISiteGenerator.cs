﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fabula.Api
{
    public interface ISiteGenerator
    {
        IEnumerable<IPageGenerator<object>> GetPages();
    }
}
