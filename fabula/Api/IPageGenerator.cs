﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fabula.Api
{
    public interface IPageGenerator<TModel>
    {
        string Path { get; }
        string TemplatePath { get; set; }
        TModel Generate();
    }
}
