﻿using Fabula.Api;
using Scriban;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Fabula
{
    public class SiteGenerationProcess
    {
        private ISiteGenerator Generator { get; }
        private readonly Dictionary<string, Template> TemplateCache = new();
        public SiteGenerationProcess(Assembly asm)
        {
            var possibleTypes = asm.GetTypes().Where(t => t.IsAssignableTo(typeof(ISiteGenerator)));
            if (possibleTypes.Count() != 1)
            {
                throw new Exception("exactly one ISiteGenerator must be specified");
            }
            var generatorObj = Activator.CreateInstance(possibleTypes.First());
            if (generatorObj != null) Generator = (ISiteGenerator)generatorObj;
            if (Generator == null) throw new Exception("could not cast site generator to ISiteGenerator???? wtf");
        }

        public SiteGenerationProcess(ISiteGenerator generator)
        {
            Generator = generator;
        }

        public void Generate(DirectoryInfo output, DirectoryInfo templates)
        {
            var pages = Generator.GetPages();
            foreach (var page in pages)
            {
                var model = page.Generate();
                var templatePath = Path.Combine(templates.ToString(), page.TemplatePath);

                Template template;
                if (TemplateCache.ContainsKey(templatePath))
                    template = TemplateCache[templatePath];
                else
                    template = Template.Parse(File.ReadAllText(templatePath), templatePath);

                var result = template.Render(model);
                File.WriteAllText(Path.Combine(output.ToString(), page.Path), result);
            }
        }
    }
}
